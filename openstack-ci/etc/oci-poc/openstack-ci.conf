
# Should we do set -e
DEBUG=yes

# Path to the OpenStack credentials file
OPENSTACK_CREDENTIALS=/etc/oci-poc/openstack-credentials

# Path to the ssh public key for the deployement
SSH_KEY_PATH=/etc/oci-poc/id_rsa
AUTHORIZED_KEY_PATH=/etc/oci-poc/authorized_keys

# Name of cluster and domain
CLUSTER_NAME=cl1
DOMAIN_NAME=example.com

DUMMYNET_IFNAME_PREFIX=ocidm
DUMMYNET_BRIDGE_NAME=br-oci

EXT_NETWORK_PREFIX=192.168.200
EXT_NETWORK_FIRST_IP=200
EXT_NETWORK_LAST_IP=249

# Flavors for OCI server
FLAVOR_PXE=cpu2-ram6-disk20

# Flavors for the CI roles
FLAVOR_CTRL=cpu8-ram24-disk20
FLAVOR_MON=cpu2-ram6-disk10
FLAVOR_OSD=cpu2-ram6-disk10
FLAVOR_COMP=cpu8-ram24-disk40

# Network name for the VM that will host OCI
NET_PUB_NAME=bdb-clint-pub01
ROUTER_GW_PUB_NAME=bdb-clint-pub01

# Name of the OCI VM
PXE_SERVER_NAME=oci

# Keypair name
KEYPAIR_NAME=oci-keypair

# Configuration for the private network where VMs will boot-up
NET_PRIV_NAME=oci-boot-network
SUBNET_PRIV_NAME=oci-boot-subnet
SUBNET_PRIV_PREFIX=192.168.100
SUBNET_PRIV_RANGE=${SUBNET_PRIV_PREFIX}.0
SUBNET_PRIV_CIDR=24
SUBNET_PRIV_ROUTER_NAME=oci-boot-router
SUBNET_PRIV_DNS_SERVER=9.9.9.9
SUBNET_PRIV_ROUTER_PORT=oci-router-privnet-port
SUBNET_PRIV_ROUTER_PORT_IP=${SUBNET_PRIV_PREFIX}.10

OCI_PXE_SERVER_IP_LAST_DIGIT=4
OCI_PXE_IP=${SUBNET_PRIV_PREFIX}.${OCI_PXE_SERVER_IP_LAST_DIGIT}
OCI_PXE_PORT_NAME=oci-pxe-port
OCI_API_SUFIX_START=90
OCI_CLUSTER_SUFFIX_START=100

DEBIAN_MIRROR_ADDRESS=http://deb.debian.org/debian
DEBIAN_SECURITY_MIRROR_ADDRESS=http://security.debian.org/debian-security

# Name of the image to use for booting
PXE_SERVER_IMAGE_NAME=debian-bullseye-11.1.0-amd64.qcow2

# OpenStack release name
OS_RELEASE=xena

# Size of the system HDDs in GB
SYSTEM_HDD_SIZE=20

# Size of the additional volume for roles:
# compute, cephosd, billosd, volume and swiftstore
DATA_HDD_SIZE=40

# Where to download the image?
#VM_IMAGE_FILENAME=debian-11-genericcloud-amd64.qcow2
VM_IMAGE_FILENAME=cirros-0.5.1-x86_64-disk.img
#VM_IMAGE_URL=http://cdimage.debian.org/cdimage/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2
VM_IMAGE_URL=http://download.cirros-cloud.net/0.5.1/cirros-0.5.1-x86_64-disk.img

# Name of the ssh user in the image
#IMAGE_SSH_USER=debian
IMAGE_SSH_USER=cirros

# Components to provision
# Note: controller is always on, so there's no switch for it.
OCI_COMPUTE_NODES=yes
OCI_NETWORK_NODES=yes

OCI_VOLUME_NODES=yes

OCI_SWIFT_NODES=no
OCI_CEPH_NODES=no
OCI_MESSAGING_NODES=no
OCI_BILLCEPH_NODES=no

# Always leave a tempest node, otherwise, no way to run tests
OCI_TEMPEST_NODES=yes
