#!/bin/sh

set -e
#set -x

if ! [ -r /etc/oci-poc/oci-poc.conf ] ; then
	echo "Cannot read /etc/oci-poc/oci-poc.conf"
fi
. /etc/oci-poc/oci-poc.conf

# This installs the cluster + add controllers:
oci-poc-install-cluster

echo "===> Adding compute nodes to cl1"
# 2x Compute (with Ceph OSD hyperconverged)
ocicli machine-add C4 cl1 compute reg-1-zone-1
ocicli machine-add C5 cl1 compute reg-2-zone-1
ocicli machine-add C6 cl1 compute reg-2-zone-1

echo "===> Setting compute nodes to use Ceph"
ocicli machine-set C4 --use_ceph_if_available yes
ocicli machine-set C5 --use_ceph_if_available yes
ocicli machine-set C6 --use_ceph_if_available yes

#echo "===> Addig Ceph OSD feature to compute nodes"
#ocicli machine-set C4 --compute-is-cephosd yes
#ocicli machine-set C5 --compute-is-cephosd yes

# Lower the reserved memory, because we don't have much available
echo "===> Setting nova-reserved-host-memory-mb to 8192"
ocicli machine-set C4 --nova-reserved-host-memory-mb 8192
ocicli machine-set C5 --nova-reserved-host-memory-mb 8192
ocicli machine-set C6 --nova-reserved-host-memory-mb 8192

echo "===> Adding network nodes to cl1"
# 2x Network
ocicli machine-add C7 cl1 network reg-1-zone-1
ocicli machine-add C8 cl1 network reg-2-zone-1

echo "===> Adding swiftproxy nodes to cl1"
# 1x Swift proxy
ocicli machine-add C9 cl1 swiftproxy reg-1-zone-1

echo "===> Adding cephmon nodes to cl1"
# 3x Ceph Mon
ocicli machine-add CA cl1 cephmon reg-1-zone-1
ocicli machine-add CB cl1 cephmon reg-2-zone-1
ocicli machine-add CC cl1 cephmon reg-2-zone-2

echo "===> Adding swiftstore nodes to cl1"
# 5x Swift Store
ocicli machine-add CD cl1 swiftstore reg-1-zone-1
ocicli machine-add CE cl1 swiftstore reg-2-zone-1
ocicli machine-add CF cl1 swiftstore reg-2-zone-1
ocicli machine-add D0 cl1 swiftstore reg-2-zone-2
ocicli machine-add D1 cl1 swiftstore reg-2-zone-2

echo "===> Adding volume nodes to cl1"
# 2x Volume
ocicli machine-add D2 cl1 volume reg-1-zone-1
ocicli machine-add D3 cl1 volume reg-2-zone-1

echo "===> Adding messaging nodes to cl1"
# 3x Messaging
ocicli machine-add D4 cl1 messaging reg-1-zone-1
ocicli machine-add D5 cl1 messaging reg-2-zone-1
ocicli machine-add D6 cl1 messaging reg-2-zone-1

echo "===> Adding a tempest node to cl1"
# 1x tempest
ocicli machine-add D7 cl1 tempest reg-2-zone-1

echo "===> Adding billmon nodes"
ocicli machine-add D8 cl1 billmon reg-1-zone-1
ocicli machine-add D9 cl1 billmon reg-2-zone-1
ocicli machine-add DA cl1 billmon reg-2-zone-2

echo "===> Adding billosd nodes"
ocicli machine-add DB cl1 billosd reg-1-zone-1
ocicli machine-add DC cl1 billosd reg-2-zone-1
ocicli machine-add DD cl1 billosd reg-2-zone-2

echo "===> Adding cephosd nodes"
ocicli machine-add DE cl1 cephosd reg-1-zone-1
ocicli machine-add DF cl1 cephosd reg-1-zone-1
ocicli machine-add E0 cl1 cephosd reg-2-zone-1
ocicli machine-add E1 cl1 cephosd reg-2-zone-1
ocicli machine-add E2 cl1 cephosd reg-2-zone-2
ocicli machine-add E3 cl1 cephosd reg-2-zone-2

echo "===> Adding DNS nodes"
ocicli machine-add E4 cl1 dns reg-1-zone-1
ocicli machine-add E5 cl1 dns reg-2-zone-2

echo "===> Reduce the (default) number of fragment in swift"
ocicli cluster-set cl1 --swift-ec-num-data-fragments 6 --swift-ec-num-parity-fragments 2 --swift-ec-enable no --install-designate yes --disable-notifications yes --glance-prefer-ceph-over-swift no

echo "===> Setting-up cl1-swiftproxy-1.infomaniak.ch as swift-proxy-hostname"
ocicli cluster-set cl1 --swift-proxy-hostname cl1-swiftproxy-1.infomaniak.ch

echo "===> Enabling swift middlewares"
ocicli cluster-set cl1 --swift-public-cloud-middlewares yes

echo "===> Calculating swift ring"
# Calculate ring
ocicli swift-calculate-ring cl1 100 100 100

exit 0
